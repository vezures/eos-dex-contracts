#!/bin/bash

rm -rf contracts/exchange/*.abi
rm -rf contracts/exchange/*.wasm
rm -rf contracts/exchange/*.wast

eosiocpp -o contracts/exchange/exchange.wast contracts/exchange/exchange.cpp
eosiocpp -g contracts/exchange/exchange.abi contracts/exchange/exchange.cpp
